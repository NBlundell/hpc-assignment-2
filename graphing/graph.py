
import matplotlib.pyplot as plt


x1 = [65536, 225000, 262144, 442368, 1048576, 2263520, 5992448]
# x1 = [9, 25, 49, 81, 121]

y1 = [10.56, 10.59, 10.62, 10.51, 10.55, 10.5, 10.5]
plt.plot(x1, y1, label = "serial", color = 'xkcd:crimson')

# y2 = [1300.44, 1416.87, 1437.2, 1459.97, 1475.62, 1470.2, 1486.08]
# plt.plot(x1, y2, label = "naive", color = 'xkcd:green')

# y3 = [3120.76, 3886.37, 4174.44, 4379.45, 4611.21, 4670.91, 4719.95]
# plt.plot(x1, y3, label = 'constant', color = 'xkcd:goldenrod')

# y4 = [1482.36, 1730.81, 1771.27, 1826.46, 1861.16, 1860.84, 1883.83]
# plt.plot(x1, y4, label = 'shared', color = 'xkcd:fuchsia')

# y5 = [1413.59, 1639.97, 1697.84, 1728.16, 1771.86, 1749.07, 1800.07]
# plt.plot (x1, y5, label = 'texture', color = 'xkcd:azure')



plt.xlabel('Image size (pixels)')

plt.ylabel('Mpixels/seconds')

# plt.title('Serial and parallel convolution run time versus filter size')
# plt.title('Parallel convolution overhead time versus image size')

# show a legend on the plot
# plt.legend(bbox_to_anchor=(0.95, 0.85))
plt.legend()

# function to show the plot
# plt.show()
plt.savefig('graph.png')
