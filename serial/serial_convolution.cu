#include <stdlib.h>
#include <stdio.h>

#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

// Constants
const char *inName = "image21.pgm";
const int filtSize = 3;
const int filtType = 0; // 0 = blur, 1 = sharp, 2 = edge

void makeFilter(float *f);

void convolution(float *inImg, float *outImg, float *filter, int w, int h) {

// result of convolution
  float sum;

// for loop for each pixel
  for (int p = 0; p < h*w; p++) {

// get 2D coordinates of 1D array
    int row = p/w;
    int col = p - row*w;
    int d = filtSize/2;

// set sum to zero before doing convolution
    sum = 0.0;

    int imgIndex;
    int filtIndex = 0;

    for (int i = row-d; i < row+d+1; i++) {
      for (int j = col-d; j < col+d+1; j++) {

        if (i >= 0 && j >= 0 && i < h && j < w) {
          imgIndex = i*w + j;
          sum += inImg[imgIndex]*filter[filtIndex];
        }
        filtIndex++;

      }
    }

// clamp between 0 and 1 and save result in output array
    if (sum > 1) {
      sum = 1.0;
    } else if (sum < 0) {
      sum = 0.0;
    }

    outImg[p] = sum;


  }

}


int main(int argc, char **argv) {

// timing overhead
  checkCudaErrors(cudaDeviceSynchronize());
  StopWatchInterface *timerO = NULL;
  sdkCreateTimer(&timerO);
  sdkStartTimer(&timerO);

  printf("Starting serial convolution...\n");

// load image
  float *inImg = NULL;
  unsigned int width, height;
  char *imgPath = sdkFindFilePath(inName, argv[0]);

  if (imgPath == NULL)
  {
      printf("Unable to source image file: %s\n", inName);
      exit(EXIT_FAILURE);
  }

  sdkLoadPGM(imgPath, &inImg, &width, &height);
  printf("Loaded '%s', %d x %d pixels\n", inName, width, height);

// allocate output image
  unsigned int size = width * height * sizeof(float);
  float *outImg = (float *) malloc(size);

// create filter
  float *filter = (float *) malloc(filtSize*filtSize*sizeof(float));
  makeFilter(filter);

// warm up run
  // convolution(inImg, outImg, filter, width, height);

// timing
  checkCudaErrors(cudaDeviceSynchronize());
  StopWatchInterface *timer = NULL;
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);

// serial convolution
  convolution(inImg, outImg, filter, width, height);

// more timing
  checkCudaErrors(cudaDeviceSynchronize());
  sdkStopTimer(&timer);
  printf("Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
  printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
  float pt = sdkGetTimerValue(&timer);
  sdkDeleteTimer(&timer);

// write result to file
  char outputFilename[1024];
  strcpy(outputFilename, imgPath);
  strcpy(outputFilename + strlen(imgPath) - 4, "_serial.pgm");
  sdkSavePGM(outputFilename, outImg, width, height);
  printf("Wrote '%s'\n", outputFilename);

// free memory
  free(imgPath);
  free(outImg);


  checkCudaErrors(cudaDeviceSynchronize());
  sdkStopTimer(&timerO);
  float t = sdkGetTimerValue(&timerO);
  t = t - pt;
  printf("Overhead time: %f (ms)\n", t);
  sdkDeleteTimer(&timerO);

}

void makeFilter(float *f) {

  if (filtType == 0) {

    for (int i = 0; i < filtSize*filtSize; i++) {
      f[i] = 1.0/9.0f;
    }

  } else if (filtType == 1) {

    for (int i = 0; i < filtSize*filtSize; i++) {
      f[i] = -1.0f;
    }
    int index = filtSize*filtSize/2;
    f[index] = (float) (filtSize*filtSize);

  } else {

    f[0] = -1.0f;
    f[1] = 0.0f;
    f[2] = 1.0f;
    f[3] = -2.0f;
    f[4] = 0.0f;
    f[5] = 2.0f;
    f[6] = -1.0f;
    f[7] = 0.0f;
    f[8] = 1.0f;

  }


}
