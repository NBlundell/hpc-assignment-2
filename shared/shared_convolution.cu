#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

// Constants
const char *inName = "image21.pgm";
const int filtSize = 3;
const int filtType = 0; // 0 = blur, 1 = sharp, 2 = edge
const int blockSize = 32;
const int tileSize = blockSize + filtSize - 1;


void makeFilter(float *f);


__global__ void sharedConvolutionKernel(float *inImg, float *outImg, float *filter, int w, int h) {

// x = COL, y = ROW
    int x = blockIdx.x*blockDim.x + threadIdx.x;
    int y = blockIdx.y*blockDim.y + threadIdx.y;

    int blockX = threadIdx.x;
    int blockY = threadIdx.y;

// shared memory allocation
    __shared__ float shInput[tileSize][tileSize];

// get 1D index for entire image and for tile
    int p = x + y*w;                                       // 1D index in terms of whole image
    int blockPixel = blockX + blockY*blockSize;             // 1D index in terms of block

// variables used in for loop below
    int d = filtSize/2;
    int bx, by, ix, iy, ind;
    int stride = blockSize*blockSize;

// since shared memory tile is greater than number of threads (block), each thread must allocate more
// than one element of shared memeory. this is done in a for loop
    for (int q = blockPixel; q < tileSize*tileSize; q += stride) {
// convert back to block 2D
      by = q/tileSize;
      bx = q - by*tileSize;

// convert to image 2D
      ix = bx + blockIdx.x*blockDim.x - d;
      iy = by + blockIdx.y*blockDim.y - d;

// check if within bounds and copy to shmem
      if (ix < 0 || iy < 0 || ix >= w || iy >= h) {
        shInput[by][bx] = 0.0;
      } else {
        ind = ix + iy*w;
        shInput[by][bx] = inImg[ind];
      }

    }

    __syncthreads();


// start of convolution
    if (x < w && y < h) {

  // set sum to zero before doing convolution
      float sum = 0.0;
      int filtIndex = 0;

      blockX += d;
      blockY += d;

      for (int i = blockY-d; i < blockY+d+1; i++) {
        for (int j = blockX-d; j < blockX+d+1; j++) {

          sum += shInput[i][j]*filter[filtIndex];

          filtIndex++;

        }
      }

  // clamp between 0 and 1 and save result in output array

      if (sum > 1) {
        sum = 1.0;
      } else if (sum < 0) {
        sum = 0.0;
      }

      outImg[p] = sum;

    }


}


int main(int argc, char **argv) {

// timing overhead
  checkCudaErrors(cudaDeviceSynchronize());
  StopWatchInterface *timerO = NULL;
  sdkCreateTimer(&timerO);
  sdkStartTimer(&timerO);

  printf("Starting shared memory convolution...\n");

  int devID = findCudaDevice(argc, (const char **) argv);

// load image
  float *inImg = NULL;
  unsigned int width, height;
  char *imgPath = sdkFindFilePath(inName, argv[0]);

  if (imgPath == NULL)
  {
      printf("Unable to source image file: %s\n", inName);
      exit(EXIT_FAILURE);
  }

  sdkLoadPGM(imgPath, &inImg, &width, &height);
  printf("Loaded '%s', %d x %d pixels\n", inName, width, height);

// allocate output image
  unsigned int size = width * height * sizeof(float);
  float *outImg = (float *) malloc(size);

// create filter
  float *filter = (float *) malloc(filtSize*filtSize*sizeof(float));
  makeFilter(filter);

// allocate arrays on device
  float *dInput = NULL;
  float *dOutput = NULL;
  float *dFilter = NULL;

  // checkCudaErrors(cudaMalloc((void **) &dInput, size));
  checkCudaErrors(cudaMalloc((void **) &dInput, size));
  checkCudaErrors(cudaMalloc((void **) &dOutput, size));
  checkCudaErrors(cudaMalloc((void **) &dFilter, filtSize*filtSize*sizeof(float)));

// copy to device
  checkCudaErrors(cudaMemcpy(dInput, inImg, size, cudaMemcpyHostToDevice));
  checkCudaErrors(cudaMemcpy(dFilter, filter, filtSize*filtSize*sizeof(float), cudaMemcpyHostToDevice));


// kernel config
  dim3 dimBlock(blockSize, blockSize);
  dim3 dimGrid(ceil(width/(float)dimBlock.x), ceil(height/(float)dimBlock.y));

// warm up run
  sharedConvolutionKernel<<<dimGrid, dimBlock>>>(dInput, dOutput, dFilter, width, height);

// timing
  checkCudaErrors(cudaDeviceSynchronize());
  StopWatchInterface *timer = NULL;
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);

// run the kernel
  sharedConvolutionKernel<<<dimGrid, dimBlock>>>(dInput, dOutput, dFilter, width, height);

// Check if kernel execution generated an error
  getLastCudaError("Kernel execution failed");

// more timing
  checkCudaErrors(cudaDeviceSynchronize());
  sdkStopTimer(&timer);
  printf("Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
  printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
  float pt = sdkGetTimerValue(&timer);
  sdkDeleteTimer(&timer);

// copy result to host
  checkCudaErrors(cudaMemcpy(outImg, dOutput, size, cudaMemcpyDeviceToHost));

// Write result to file
  char outputFilename[1024];
  strcpy(outputFilename, imgPath);
  strcpy(outputFilename + strlen(imgPath) - 4, "_shared.pgm");
  sdkSavePGM(outputFilename, outImg, width, height);
  printf("Wrote '%s'\n", outputFilename);

// free memory
  checkCudaErrors(cudaFree(dInput));
  checkCudaErrors(cudaFree(dOutput));
  checkCudaErrors(cudaFree(dFilter));
  free(imgPath);
  free(outImg);

  checkCudaErrors(cudaDeviceSynchronize());
  sdkStopTimer(&timerO);
  float t = sdkGetTimerValue(&timerO);
  t = t - pt;
  printf("Overhead time: %f (ms)\n", t);
  sdkDeleteTimer(&timerO);

}


void makeFilter(float *f) {

  if (filtType == 0) {

    for (int i = 0; i < filtSize*filtSize; i++) {
      f[i] = 1.0/9.0f;
    }

  } else if (filtType == 1) {

    for (int i = 0; i < filtSize*filtSize; i++) {
      f[i] = -1.0f;
    }
    int index = filtSize*filtSize/2;
    f[index] = (float) (filtSize*filtSize);

  } else {

    f[0] = -1.0f;
    f[1] = 0.0f;
    f[2] = 1.0f;
    f[3] = -2.0f;
    f[4] = 0.0f;
    f[5] = 2.0f;
    f[6] = -1.0f;
    f[7] = 0.0f;
    f[8] = 1.0f;

  }


}
