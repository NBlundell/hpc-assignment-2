#include <stdlib.h>
#include <stdio.h>
#include <stdio.h>

#include <cuda_runtime.h>
#include <helper_functions.h>
#include <helper_cuda.h>

// Constants
const char *inName = "image21.pgm";
const int filtSize = 3;
const int filtType = 0; // 0 = blur, 1 = sharp, 2 = edge


texture<float, 2, cudaReadModeElementType> tex;


void makeFilter(float *f);


__global__ void textureConvolutionKernel(float *outImg, float *filter, int w, int h) {

// x = COL, y = ROW
    int x = blockIdx.x*blockDim.x + threadIdx.x;
    int y = blockIdx.y*blockDim.y + threadIdx.y;


    if (x < w && y < h) {

      int d = filtSize/2;

  // set sum to zero before doing convolution
      float sum = 0.0f;
      int filtIndex = 0;

      for (int i = y-d; i < y+d+1; i++) {
        for (int j = x-d; j < x+d+1; j++) {

          if (i >= 0 && j >= 0 && i < h && j < w) {
            float u = (i+0.5f); // (float) w;
            float v = (j+0.5f); // (float) h;
            sum += tex2D(tex, v, u)*filter[filtIndex];
          }
          filtIndex++;

        }
      }

// clamp between 0 and 1 and save result in output array

      if (sum > 1) {
        sum = 1.0;
      } else if (sum < 0) {
        sum = 0.0;
      }

      int p = x + y*w;
      outImg[p] = sum;

    }


}


int main(int argc, char **argv) {

// timing overhead
  checkCudaErrors(cudaDeviceSynchronize());
  StopWatchInterface *timerO = NULL;
  sdkCreateTimer(&timerO);
  sdkStartTimer(&timerO);

  printf("Starting texture memory convolution...\n");

  int devID = findCudaDevice(argc, (const char **) argv);

// load image
  float *inImg = NULL;
  unsigned int width, height;
  char *imgPath = sdkFindFilePath(inName, argv[0]);

  if (imgPath == NULL)
  {
      printf("Unable to source image file: %s\n", inName);
      exit(EXIT_FAILURE);
  }

  sdkLoadPGM(imgPath, &inImg, &width, &height);
  printf("Loaded '%s', %d x %d pixels\n", inName, width, height);

// allocate output image
  unsigned int size = width * height * sizeof(float);
  float *outImg = (float *) malloc(size);

// create filter
  float *filter = (float *) malloc(filtSize*filtSize*sizeof(float));
  makeFilter(filter);

// allocate arrays on device
  // float *dInput = NULL;
  float *dOutput = NULL;
  float *dFilter = NULL;

  // checkCudaErrors(cudaMalloc((void **) &dInput, size));
  checkCudaErrors(cudaMalloc((void **) &dOutput, size));
  checkCudaErrors(cudaMalloc((void **) &dFilter, filtSize*filtSize*sizeof(float)));

// copy to device
  checkCudaErrors(cudaMemcpy(dFilter, filter, filtSize*filtSize*sizeof(float), cudaMemcpyHostToDevice));

  cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
  cudaArray *cuArray;
  checkCudaErrors(cudaMallocArray(&cuArray, &channelDesc, width, height));
  checkCudaErrors(cudaMemcpyToArray(cuArray, 0, 0, inImg, size, cudaMemcpyHostToDevice));

// Set texture parameters
  tex.addressMode[0] = cudaAddressModeWrap;
  tex.addressMode[1] = cudaAddressModeWrap;
  tex.filterMode = cudaFilterModeLinear;
  tex.normalized = false;    // access with normalized texture coordinates

// Bind the array to the texture
  checkCudaErrors(cudaBindTextureToArray(tex, cuArray, channelDesc));

// kernel config
  dim3 dimBlock(32,32);
  dim3 dimGrid(ceil(width / 32.0), ceil(height / 32.0));

// warm up run
  textureConvolutionKernel<<<dimGrid, dimBlock>>>(dOutput, dFilter, width, height);

// timing
  checkCudaErrors(cudaDeviceSynchronize());
  StopWatchInterface *timer = NULL;
  sdkCreateTimer(&timer);
  sdkStartTimer(&timer);

// run the kernel
  textureConvolutionKernel<<<dimGrid, dimBlock>>>(dOutput, dFilter, width, height);

// Check if kernel execution generated an error
  getLastCudaError("Kernel execution failed");

// more timing
  checkCudaErrors(cudaDeviceSynchronize());
  sdkStopTimer(&timer);
  printf("Processing time: %f (ms)\n", sdkGetTimerValue(&timer));
  printf("%.2f Mpixels/sec\n", (width *height / (sdkGetTimerValue(&timer) / 1000.0f)) / 1e6);
  float pt = sdkGetTimerValue(&timer);
  sdkDeleteTimer(&timer);

// copy result to host
  checkCudaErrors(cudaMemcpy(outImg, dOutput, size, cudaMemcpyDeviceToHost));

// Write result to file
  char outputFilename[1024];
  strcpy(outputFilename, imgPath);
  strcpy(outputFilename + strlen(imgPath) - 4, "_texture.pgm");
  sdkSavePGM(outputFilename, outImg, width, height);
  printf("Wrote '%s'\n", outputFilename);

// free memory
  checkCudaErrors(cudaFreeArray(cuArray));
  checkCudaErrors(cudaFree(dOutput));
  checkCudaErrors(cudaFree(dFilter));
  cudaUnbindTexture(tex);
  free(imgPath);
  free(outImg);


  checkCudaErrors(cudaDeviceSynchronize());
  sdkStopTimer(&timerO);
  float t = sdkGetTimerValue(&timerO);
  t = t - pt;
  printf("Overhead time: %f (ms)\n", t);
  sdkDeleteTimer(&timerO);

}


void makeFilter(float *f) {

  if (filtType == 0) {

    for (int i = 0; i < filtSize*filtSize; i++) {
      f[i] = 1.0/9.0f;
    }

  } else if (filtType == 1) {

    for (int i = 0; i < filtSize*filtSize; i++) {
      f[i] = -1.0f;
    }
    int index = filtSize*filtSize/2;
    f[index] = (float) (filtSize*filtSize);

  } else {

    f[0] = -1.0f;
    f[1] = 0.0f;
    f[2] = 1.0f;
    f[3] = -2.0f;
    f[4] = 0.0f;
    f[5] = 2.0f;
    f[6] = -1.0f;
    f[7] = 0.0f;
    f[8] = 1.0f;

  }


}
